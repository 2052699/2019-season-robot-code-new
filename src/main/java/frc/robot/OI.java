/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.robot.RobotMap.eJoyStick;
import frc.robot.RobotMap.eJoyStickPorts;
import frc.robot.commands.ElevDownCommand;
import frc.robot.commands.ElevUpCommand;
import frc.robot.commands.RGBaCommand;
import frc.robot.commands.RGBbCommand;
import frc.robot.commands.RGBxCommand;
import frc.robot.commands.RGByCommand;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
  //// CREATING BUTTONS
  // One type of button is a joystick button which is any button on a
  //// joystick.
  // You create one by telling it which joystick it's on and which button
  // number it is.
  // Joystick stick = new Joystick(port);

  // There are a few additional built in buttons you can use. Additionally,
  // by subclassing Button you can create custom triggers and bind those to
  // commands the same as any other Button.

  //// TRIGGERING COMMANDS WITH BUTTONS
  // Once you have a button, it's trivial to bind it to a button in one of
  // three ways:

  // Start the command when the button is pressed and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenPressed(new ExampleCommand());

  // Run the command while the button is being held down and interrupt it once
  // the button is released.

  // Start the command when the button is released and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenReleased(new ExampleCommand());

  Joystick DriveJStick = new Joystick(eJoyStick.ChannelOneRC);  //Creates Joystick
  Joystick ManipJStick = new Joystick(eJoyStick.ChannelTwoRC);
  Button l1Button = new JoystickButton(ManipJStick, eJoyStickPorts.L1_BUTTON);    //Creates Button
  Button r1Button = new JoystickButton(ManipJStick, eJoyStickPorts.R1_BUTTON);
  
  Button aButton = new JoystickButton(ManipJStick, eJoyStickPorts.kA);
  Button xButton = new JoystickButton(ManipJStick, eJoyStickPorts.kX);
  Button bButton = new JoystickButton(ManipJStick, eJoyStickPorts.kB);
  Button yButton = new JoystickButton(ManipJStick, eJoyStickPorts.kY);
  

  public Joystick getDriveJoystick() {
    return DriveJStick;
  }

  public Joystick getManipJoystick() {
    return ManipJStick;
  }

  public OI() {
    l1Button.whileHeld(new ElevUpCommand());   //While Button l1 is held, execute ElevUpCommand
    r1Button.whileHeld(new ElevDownCommand());

    aButton.whileHeld(new RGBaCommand());
    xButton.whileHeld(new RGBxCommand());
    bButton.whileHeld(new RGBbCommand());
    yButton.whileHeld(new RGByCommand());
  }


}
