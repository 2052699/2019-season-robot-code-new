/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Add your docs here.
 */
public class RGBsystem extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private Relay lightRelay;
  private Spark blinkin;

  public void LightsA() {
    lightRelay = new Relay(9);

    lightRelay.set(Relay.Value.kOn);

    blinkin = new Spark(6);
    blinkin.set(-0.99);
  }

  public void LightsB() {
    lightRelay = new Relay(9);

    lightRelay.set(Relay.Value.kOn);

    blinkin = new Spark(6);
    blinkin.set(-0.33);
  }

  public void LightsX() {
    lightRelay = new Relay(9);

    lightRelay.set(Relay.Value.kOn);

    blinkin = new Spark(6);
    blinkin.set(0.33);
  }

  public void LightsY() {
    lightRelay = new Relay(9);

    lightRelay.set(Relay.Value.kOn);

    blinkin = new Spark(6);
    blinkin.set(0.99);
  }

  public void LightsReg() {
    lightRelay = new Relay(9);

    lightRelay.set(Relay.Value.kOn);

    blinkin = new Spark(6);
    blinkin.set(0.55);
  }



  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }
}
