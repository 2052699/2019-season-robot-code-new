/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.RobotMap.eCam;
import frc.robot.RobotMap.eJoyStickPorts;
import frc.robot.commands.CamByJoystick;

/**
 * Add your docs here.
 */
public class CamServoSystem extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private DifferentialDrive m_differentialCamDrive;

public CamServoSystem() {
  VictorSP m_camHorizontalController = new VictorSP(eCam.CamHorzChannel);
  VictorSP m_camVerticalController = new VictorSP(eCam.CamVertChannel);

// Invert the left side motors.
// You may need to change or remove this to match your robot.
// m_leftWebController.setInverted(eInvertMotor.eLeftmotor);
// m_rightWebController.setInverted(eInvertMotor.eRightmotor);

addChild("horizontalCamController",m_camHorizontalController);
addChild("verticalCamController",m_camVerticalController);

m_differentialCamDrive = new DifferentialDrive(m_camHorizontalController, m_camVerticalController);
addChild("DifferentialDrive",m_differentialCamDrive);

}



  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new CamByJoystick()); //calls the command "camByJoystick"
  }

public void stop() {
  m_differentialCamDrive.tankDrive(0, 0);
}

public void camGo(double leftSpeed, double rightSpeed) {
  m_differentialCamDrive.tankDrive(leftSpeed, rightSpeed, true);
}

public void camGo(Joystick manipJoystick) {

  this.camGo(manipJoystick.getRawAxis(eJoyStickPorts.camXStickRight), manipJoystick.getRawAxis(eJoyStickPorts.camYStickRight));
}

}

// ***********Pilot/User MUST READ FOR THE CODE TO WORK***************
//By Milos:  This code will only be run by the co-pilot controller.
//           co-pilot controller = webJoyStick (in the program)
//The right stick on the Microsoft Controller will controll the Camera mount
//****************************************************************** */
