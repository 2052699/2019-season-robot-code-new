/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.RobotMap.eArm;

/**
 * Add your docs here.
 */
public class ArmLiftSystem extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private VictorSP m_liftController = new VictorSP(eArm.ArmChannel);

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }

  public void ElevatorUp() {
    m_liftController.set(RobotMap.ELEV_UP_SPEED);
  }

  public void ElevatorDown() {
    m_liftController.set(RobotMap.ELEV_DOWN_SPEED);
  }
//sets motor speed to the value assigned in RobotMap while button is held
  public void StopElevator() {
    m_liftController.set(0);
  }
  
}

// While Held (trigger)
// up/down - 2 commands - 2 functions
