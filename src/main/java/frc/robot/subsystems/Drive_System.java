/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.RobotMap.eInvertMotor;
import frc.robot.RobotMap.eJoyStickPorts;
import frc.robot.RobotMap.eWheelDrive;
import frc.robot.commands.Drive_By_Joystick;

/**
 * An example subsystem. You can replace me with your own Subsystem.
 */
public class Drive_System extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private VictorSP m_leftController;
  private VictorSP m_rightController;
  private DifferentialDrive m_differentialDrive;
  private SpeedControllerGroup leftControllerGroup;
  private SpeedControllerGroup rightControllerGroup;

  public Drive_System() {

    VictorSP m_leftController = new VictorSP(eWheelDrive.leftChannel);
    VictorSP m_leftController1 = new VictorSP(eWheelDrive.leftChannel2);
    VictorSP m_rightController = new VictorSP(eWheelDrive.rightChannel);
    VictorSP m_rightController2 = new VictorSP(eWheelDrive.rightChannel2);

    leftControllerGroup = new SpeedControllerGroup(m_leftController, m_leftController1);
    rightControllerGroup = new SpeedControllerGroup(m_rightController, m_rightController2);

    // Invert setting if needed
    // change invert true/false (in RobotMap.java) to change what forward is on motor
    m_leftController.setInverted(eInvertMotor.eLeftmotor);
    m_rightController.setInverted(eInvertMotor.eRightmotor);

    addChild("leftController", m_leftController);
    addChild("rightController", m_rightController);

    m_differentialDrive = new DifferentialDrive(m_leftController, m_rightController);
    addChild("DifferentialDrive", m_differentialDrive);
    m_differentialDrive.setSafetyEnabled(true);
    m_differentialDrive.setExpiration(0.2);
    m_differentialDrive.setMaxOutput(1.0);

  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
    setDefaultCommand(new Drive_By_Joystick());
  }

  // To Stop the drivetrain from moveing

  public void stop() {
    m_differentialDrive.arcadeDrive(0, 0);
  }

  // Set drive speed by axis

  public void go(double leftSpeed, double rightSpeed) {
    if (Math.abs(leftSpeed) < 0.15) {
      leftSpeed = 0;
    }     //allows for error in off centered joysticks, so robot doesn't move for miniscule joystick movements of +-0.15
    if (Math.abs(rightSpeed) < 0.15) {
      rightSpeed = 0;
    }

    m_differentialDrive.arcadeDrive(leftSpeed, rightSpeed, true);
  }

  // run the drivetrain by the joistick
  public void go(Joystick joyStick) {
    this.go(joyStick.getRawAxis(eJoyStickPorts.LEFT_Y_AXIS), joyStick.getRawAxis(eJoyStickPorts.RIGHT_X_AXIS));
  }

// Arcade Drive class allowes for clasic "video game style" driving (one stick for forward/back and other for left/right)
// Tank Drive Class is used to drive robot with 2 sticks as a skid steer

}
