/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
  // For example to map the left and right motors, you could define the
  // following variables to use with your drivetrain subsystem.
  // public static int leftMotor = 1;
  // public static int rightMotor = 2;

  // If you are using multiple modules, make sure to define both the port
  // number and the module. For example you with a rangefinder:
  // public static int rangefinderPort = 1;
  // public static int rangefinderModule = 1;

  //Adjust Elevator speed

  public static final double ELEV_UP_SPEED = 0.8;
  public static final double ELEV_DOWN_SPEED = -0.6;

//Invert Motors to accomodate hardware

  public static class eInvertMotor{
  
  public static final boolean eRightmotor = true;
  public static final boolean eLeftmotor = false; 

  }
//Assign PWM ports to lable
  public static class ePWM {
    public static final int pwm0 = 0;
    public static final int pwm1 = 1;
    public static final int pwm2 = 2;
    public static final int pwm3 = 3;
    public static final int pwm4 = 4;
    public static final int pwm5 = 5;
    public static final int pwm6 = 6;
    public static final int pwm7 = 7;
    public static final int pwm8 = 8;
  }

//Assign PWM port to motor controller

  public static class eWheelDrive {
    public static final int leftChannel = ePWM.pwm1;
    public static final int leftChannel2 = ePWM.pwm2;

    public static final int rightChannel = ePWM.pwm4;
    public static final int rightChannel2 = ePWM.pwm5;
  }

//Assigns PWM chanel to Elevator motor controller
  public static class eArm {
    public static final int ArmChannel = ePWM.pwm3;
  }

public static class eCam {
  public static final int CamVertChannel = ePWM.pwm7;
  public static final int CamHorzChannel = ePWM.pwm8;
}

public static class eLight {
  public static final int LightChannel1 = ePWM.pwm6;
}

// Assigns Player Controller to USB port number on FRC Driver Station

  public static class eJoyStick {
    public static final int ChannelOneRC = 0;
    public static final int ChannelTwoRC = 1;
  }

  //XBOX Joystick ports
  public static class eJoyStickPorts {
    public static final int kStickLeft = 1;
    public static final int kStickRight = 4;
    
    public static final int LEFT_Y_AXIS = 1;
    public static final int RIGHT_X_AXIS = 4;
    public static final int L1_BUTTON = 5;
    public static final int R1_BUTTON = 6;

    public static final int camXStickRight = 4;
    public static final int camYStickRight = 5;

    public static final int kA  =  1;
    public static final int kB  =  2;
    public static final int kX  =  3;
    public static final int kY  =  4;

    //public static final int kTriggerLeft = 9;
    //public static final int kTriggerRight =  10;
    //public static final int kStickLeft  =  1;
    //public static final int webXtickLeft = 0;
    //public static final int webXStickRight = 4;
    //public static final int webYtickLeft = 1;
    //public static final int webYStickRight = 5;
    //public static final int kStickRight  = 5 ;
    //public static final int kA  =  1;
    // public static final int kB  =  2;
    // public static final int kX  =  3;
    // public static final int kY  =  4;
    // public static final int kBack  =  7;
    // public static final int kStart  =  8;
  }
//


 
}
